// Описать своими словами в несколько строчек, зачем в программировании нужны циклы. Циклы нужны для того, чтобы не повторять набор однотипных действий много раз подряд.
//Например, нам нужно вывести 100 чисел подряд, для того чтобы не выводить каждое число по порядку "вручную" можно написать программу, (задав условие) которая автоматически выведет числа от 1 до 100.



// let userNumber = Number;  // Number
let countNumbers = [];  // array: https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array


function getNumber(userNumber) {
    userNumber = prompt("Enter your number");
    if (userNumber) {
        console.log("user entered something");
        checkIsNumber(userNumber);
    } else {
        console.log("User had entered nothing");
        //если пользователь ничего не ввел, запускается рекурсия, которая будет работать пока юзер не введет что-либо
        getNumber();
    }
}

function checkIsNumber(userNumber) {
    userNumber = Number(userNumber);
    if (Number.isInteger(userNumber)) {
        // выяснили что это число и не строка и не дробное
        console.log("user entered an integer: " + Number.isInteger(userNumber));
        countNumbers = countUserNumber(userNumber);
    } else {
        // если юзер вводит не число или дробное число
        console.log("user entered not an integer");
        getNumber();
    }
    alert(countNumbers);
}

function countUserNumber(userNumber) {
    for (i = 0; i < userNumber; i = i + 5) {
        console.log(i);
        // Если больше нуля - сохранить в array, иначе - оставить пустым
        if (i > 0) {
            countNumbers.push(i);
        }
    }
    if (countNumbers.length === 0) alert("Sorry, no numbers");
    // End of for:
    console.log("return all numbers array: " + countNumbers);
    return (countNumbers);
}

getNumber();


//
//
//