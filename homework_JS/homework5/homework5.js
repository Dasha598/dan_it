// Опишите своими словами, что такое экранирование, и зачем оно нужно в языках программирования.
// Экранирование - если в коде нужно использовать/указать специальные символы, например такие как скобки ()
// их нужно экранировать через слэш /.

// userFirstName = "Dasha";
// userLastName = "Pislar";
// birthday = "31.10.1992".split(".");

function createNewUser () {
    let userFirstName = prompt("Enter your name");
    let userLastName = prompt("Enter your surname");
    let birthday = prompt("Enter your birthday date in the format 'dd.mm.yyyy'").split(".");
    return {
        userFirstName: userFirstName,
        userLastName: userLastName,
        // loginName: getLogin(userFirstName, userLastName),
        userAge: getAge(birthday),
        loginName: getPassword(userFirstName, userLastName, birthday)
    };
}

console.log(createNewUser());

function getAge(birthday) {
    let todayDate = new Date();
    birthday = new Date(birthday[2], birthday[1] - 1, birthday[0]);
    let diff = todayDate - birthday;
    return Math.floor(diff / 31557600000);// кол-во милисекунд в году 1000*60*60*24*365.25
}

function getPassword(userFirstName, userLastName,birthday) {
    let firstLetter = userFirstName[0].toUpperCase();
    let lowerLastName = userLastName.toLowerCase();
    return firstLetter + lowerLastName + birthday[2];
}